export const message = {
  error_access: 'Los credenciales de acceso son incorrectas',
  error_form_login_invalid: 'Usuario o contraseña no válidos.',
  error_services: 'Comunicación no disponible intente nuevamente.',
  error_load: 'Ha ocurrido un error, intente nuevamente.',
  error_search_client: 'Cliente no encontrado.',
  success_booking: 'La reserva se ha registrado exitosamente.',
  success_update_booking: 'La reserva se ha actualizado exitosamente.',
  error_equals_date: 'La fecha seleccionada ya esta asociada a otro servicio.',
  success_delete: 'Se ha eliminado la reserva exitosamente.',
  comfirm_delete: 'Usted no podra recuperar los datos eliminados.',
  password_message: '¡Cree una contraseña nueva! <br>La contraseña debe tener mínimo 8 caracteres, al menos un dígito, al menos' +
    ' una minúscula y al menos una mayúscula. Puede tener otros símbolos.',
}
