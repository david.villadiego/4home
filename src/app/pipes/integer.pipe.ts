import {Pipe, PipeTransform} from '@angular/core';

@Pipe({
  name: 'integer'
})
export class IntegerPipe implements PipeTransform {

  transform(value: string): unknown {
    return parseInt(value, 0);

  }

}
