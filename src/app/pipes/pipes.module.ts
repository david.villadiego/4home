import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {IntegerPipe} from './integer.pipe';



@NgModule({
  declarations: [IntegerPipe],
  imports: [
    CommonModule
  ],
  exports:[IntegerPipe]
})
export class PipesModule { }
