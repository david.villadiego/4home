import {Component, OnInit} from '@angular/core';
import {RSUser} from '../../interfaces/auth';
import {AuthService} from '../../services/auth/auth.service';
import {ReservationService} from '../../services/bookings/reservation.service';
import {UtilsService} from '../../services/utils/utils.service';
import {message} from '../../../environments/labels-es';
import {MenuController} from '@ionic/angular';

@Component({
  selector: 'app-history',
  templateUrl: './history.page.html',
  styleUrls: ['./history.page.scss'],
})
export class HistoryPage implements OnInit {
  loading: any;
  dataUser: RSUser = {};
  reservations: Array<any> = new Array();
  state: number
  flagView: boolean

  constructor(private authService: AuthService,
              private reservationService: ReservationService,
              private utils: UtilsService,
              public menuCtrl: MenuController) {
    this.state = 1
    this.flagView = true;
  }

  ngOnInit() {
    this.dataUser = this.authService.user;
    this.loadHistory()
  }

  ionViewDidEnter() {
    this.menuCtrl.enable(true);
  }

  async loadHistory(state?) {
    if (state) {
      this.state = state;
    }
    this.loading = await this.utils.presentLoading('Cargando');
    this.reservationService.getByStatus(this.state).subscribe(resp => {
      this.loading.dismiss();
      this.reservations = resp;
    }, error => {
      this.loading.dismiss();
      this.utils.presentAlert(message.error_load, 'Reserva', '');
    });

  }
}
