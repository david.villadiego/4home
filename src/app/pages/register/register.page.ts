import {Component, ElementRef, Input, OnInit, ViewChild} from '@angular/core';
import {message} from '../../../environments/labels-es';
import {AbstractControl, FormArray, FormBuilder, FormControl, FormGroup, ValidatorFn, Validators} from '@angular/forms';
import {RQLogin, RQRegister} from '../../interfaces/auth';
import {IonItemSliding, MenuController, NavController} from '@ionic/angular';
import {AuthService} from '../../services/auth/auth.service';
import {UtilsService} from '../../services/utils/utils.service';
import {SignupService} from '../../services/signup/signup.service';

import {map} from 'rxjs/operators';

@Component({
  selector: 'app-register',
  templateUrl: './register.page.html',
  styleUrls: ['./register.page.scss'],
})
export class RegisterPage implements OnInit {
  @ViewChild(IonItemSliding, {static: false}) slide: IonItemSliding;
  @Input() register: boolean;
  customer: RQRegister
  loading: any;
  btnRegister: string;
  form: FormGroup;

  documentTypes: Array<any> = new Array();
  customerTypes: Array<any> = new Array();

  // ----------Pattern-----------
  emailPattern: any = /^[A-Za-z0-9._%+-]{3,}@[a-zA-Z]{3,}([.]{1}[a-zA-Z]{2,}|[.]{1}[a-zA-Z]{2,}[.]{1}[a-zA-Z]{2,})/;
  passwordPattern: any = /(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}/;
  showpassword = false;
  showpasswordConfirm = false;
  submitted = false;
  addAddress = false;
  editAddress = false;
  oldAddress = null;
  authUser: RQLogin = {};
  flagEmail: boolean;
  flagIden: boolean;
  debil: boolean;
  media: boolean;
  alta: boolean;

  get addressesArray() {
    return this.form.get('addresses') as FormArray;
  }

  constructor(
    private authService: AuthService,
    private singupService: SignupService,
    private utils: UtilsService,
    private formBuilder: FormBuilder,
    private navController: NavController,
    public menuCtrl: MenuController,
    private toast: UtilsService
  ) {
    this.debil = false;
    this.media = false;
    this.alta = false;
    this.btnRegister = 'Registrar';
    this.loadForm();
  }

  ngOnInit(): void {
    this.getCustomerTypes();
  }

  ionViewDidEnter() {
    this.menuCtrl.enable(false);
  }

  loadForm() {
    this.form = this.formBuilder.group({
      customer_type: [null, [Validators.required]],
      type_document: [null, [Validators.required]],
      identification: ['', [Validators.required, Validators.maxLength(20), Validators.pattern('^[0-9]*$')],
        this.validateIdentification.bind(this)],
      name: ['', [Validators.required, Validators.maxLength(50)],],
      lastname: ['', [Validators.required, Validators.max(50)]],
      email: ['', [Validators.required, Validators.max(50), Validators.pattern(this.emailPattern)], this.validateEmail.bind(this)],
      password: ['', [Validators.required, Validators.minLength(8), Validators.maxLength(12),
        this.validatePassword.bind(this), Validators.pattern(this.passwordPattern)]],
      password_confirmation: ['', [Validators.required]],
      phone: ['', [Validators.required, Validators.pattern('^[0-9]*$')]],
      mobile: ['', [Validators.required, Validators.pattern('^[0-9]*$')]],
      contact_name: ['', [Validators.required, Validators.maxLength(250)]],
      status: [{value: true, disabled: true}, [Validators.required]],
      billing_address: ['', [Validators.required, Validators.maxLength(250)]],
      address: [''],
      addresses: this.formBuilder.array([], {validators: this.minAddresses}),
    }, {validators: this.matchingPasswords('password', 'password_confirmation')});
  }

  matchingPasswords(password: string, passwordconfirmation: string) {
    return (group: FormGroup): { [key: string]: any } => {
      const passwordT = group.controls[password];
      const confirmPassword = group.controls[passwordconfirmation];
      if (passwordT.value !== confirmPassword.value) {
        return {
          mismatchedPasswords: true
        };
      }
    };
  }

  minAddresses: ValidatorFn = (form: FormArray) => {
    return form.controls.length >= 1 ? null : {required: true};
  }


  validateIdentification(control: AbstractControl) {
    return this.singupService.validateIdentification(control.value).pipe(map((resp: any) => {
      if (Object.keys(resp).length > 0) {
        this.flagIden = true;
        return {identification: true};
      } else {
        this.flagIden = false;
        return null;
      }
    }));
  }

  validateEmail(control: AbstractControl) {
    return this.singupService.validateEmail(control.value).pipe(map((resp: any) => {
      if (Object.keys(resp).length > 0) {
        this.flagEmail = true;
        return {email: true};
      } else {
        this.flagEmail = false;
        return null;
      }
    }));
  }

  async getDocumentsType() {
    this.singupService.getWithoutAuthentication().subscribe((resp: any) => {
      resp.map((type: any) => {
        this.documentTypes.push({value: String(type.id), label: type.name});
        this.documentTypes = this.documentTypes.slice();
      });
      this.loading.dismiss();
    }, error => {
      this.utils.presentAlert(message.error_services, 'Registro', '');
    });
  }

  async getCustomerTypes() {
    this.loading = await this.utils.presentLoading('Cargando');
    this.singupService.customerType().subscribe((resp: any) => {
      resp.data.map((type: any) => {
        this.customerTypes.push({value: String(type.id), label: type.name});
        this.customerTypes = this.customerTypes.slice();
      });
      this.getDocumentsType();
    }, error => {
      this.utils.presentAlert(message.error_services, 'Registro', '');
    });
  }

  newAddress() {
    this.addAddress = true;
  }

  saveAddress() {
    if (!this.form.controls.address.value) {
      return false;
    }

    this.addressesArray.push(
      this.formBuilder.group({
        index: new FormControl(this.addressesArray.controls.length),
        address: new FormControl(this.form.controls.address.value),
      })
    )
    this.cancelAddress();
  }

  getAddress(address: any) {
    if (this.slide)
      this.slide.close();
    if (address) {
      this.form.controls.address.setValue('');
      this.addAddress = false;
      this.oldAddress = address;
      this.form.controls.address.setValue(address.value.address);
      this.editAddress = true;
    }
  }

  updateAddress() {
    if (this.slide)
      this.slide.close();
    if (this.oldAddress) {
      this.oldAddress.controls.address.setValue(this.form.controls.address.value);
      this.cancelAddress();
    }
  }

  removeAddress(item: any) {
    this.getAddress(item)
    if (this.oldAddress) {
      this.addressesArray.removeAt(this.addressesArray.value.findIndex(address => address.index === this.oldAddress.value.index));
      this.cancelAddress();
    }
  }

  cancelAddress() {
    if (this.slide)
      this.slide.close();
    this.form.controls.address.setValue('');
    this.addAddress = false;
    this.editAddress = false;
    this.oldAddress = null;
  }

  validatePassword(pass: FormGroup) {
    let password = pass.value;
    const security = this.passwordPattern;
    let cont = 0;
    while ((cont < password.length)) {
      if (password.charAt(cont) === ' ') {
        password = password.replace(' ', '');
        return;
      }
      cont++;
    }

    if (password.length >= 1 && password.length <= 4) {
      this.debil = true;
    } else {
      this.debil = false;
    }
    if (password.length > 4) {
      this.debil = true;
      this.media = true;
    } else {
      this.media = false;
    }

    if (security.test(password)) {
      this.debil = true;
      this.alta = true;
      this.media = true;
    } else {
      this.alta = false;
    }
  }

  async onSubmit() {
    debugger;
    this.submitted = true;
    if (!this.form.valid) {
      return;
    }
    this.loading = await this.utils.presentLoading('Espere');

    this.customer = this.form.value;
    this.customer.roles = ['2'];

    const listAddresses = new Array();

    this.addressesArray.value.map((address: any) => {
      listAddresses.push(address.address);
    });

    this.customer.addresses = listAddresses;

    this.singupService.post(this.customer).subscribe(async (data: any) => {

      this.authUser.email = this.customer.email;
      this.authUser.password = this.customer.password;

      const validLogin = await this.authService.login(this.authUser);

      if (validLogin) {
        this.loading.dismiss();
        this.navController.navigateRoot('/bookings',
          {
            animated: true
          });
      } else {
        this.loading.dismiss();
        this.utils.presentAlert(message.error_access, 'Ingreso', '');
      }

    }, (err) => {
      this.loading.dismiss();
      if (err.error.errors) {
        let res = '';
        Object.keys(err.error.errors).forEach((data, index) => {
          res += err.error.errors[data][0] + '<br>';
        });
        this.utils.presentAlert(res, 'Registro', '');
      }
    });
  }

  viewMessage() {
    this.toast.presentToast(message.password_message, 'Registro', 'warning', 10000);
  }
}
