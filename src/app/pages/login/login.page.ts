import {Component, OnInit} from '@angular/core';
import {RQLogin, UserDB} from '../../interfaces/auth';
import {AuthService} from '../../services/auth/auth.service';
import {UtilsService} from '../../services/utils/utils.service';
import {MenuController, NavController} from '@ionic/angular';
import {NgForm} from '@angular/forms';
import {message} from '../../../environments/labels-es';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {

  loading: any;
  dataUser: RQLogin = {};
  user: UserDB;
  showpassword = false;

  constructor(private authService: AuthService,
              private utils: UtilsService,
              private navController: NavController,
              public menuCtrl: MenuController) {
  }

  ngOnInit() {
  }

  ionViewDidEnter() {
    this.menuCtrl.enable(false);
  }

  async onSubmit(flogin: NgForm) {
    if (flogin.invalid) {
      this.utils.presentAlert(message.error_form_login_invalid, 'Ingreso', '');
      return;
    }
    this.loading = await this.utils.presentLoading('Cargando');
    const validLogin = await this.authService.login(this.dataUser);

    if (validLogin) {
      this.loading.dismiss();
      this.navController.navigateRoot('/bookings',
        {
          animated: true
        });
    } else {
      this.loading.dismiss();
      this.utils.presentAlert(message.error_access, 'Ingreso', '');
    }
  }
}
