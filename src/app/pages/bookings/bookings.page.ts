import {Component, OnInit} from '@angular/core';
import {message} from '../../../environments/labels-es';
import {RSUser} from '../../interfaces/auth';
import {AuthService} from '../../services/auth/auth.service';
import {ReservationService} from '../../services/bookings/reservation.service';
import {UtilsService} from '../../services/utils/utils.service';
import {RSBooking} from '../../interfaces/booking';
import {MenuController} from '@ionic/angular';

@Component({
  selector: 'app-bookings',
  templateUrl: './bookings.page.html',
  styleUrls: ['./bookings.page.scss'],
})
export class BookingsPage implements OnInit {

  loading: any;
  dataUser: RSUser = {};
  reservations: RSBooking[] = [];
  flagView: boolean


  constructor(private authService: AuthService,
              private reservationService: ReservationService,
              private utils: UtilsService,
              public menuCtrl: MenuController) {
    this.flagView = false;
  }

  ngOnInit() {
    this.dataUser = this.authService.user;
    this.reservationService.newReservation.subscribe(response => {
      if (response) {
        this.loadDataReservation();
      }
    });
    this.loadDataReservation();
  }

  ionViewDidEnter() {
    this.menuCtrl.enable(true);
  }

  async loadDataReservation() {
    this.loading = await this.utils.presentLoading('Cargando');
    const response = await this.reservationService.getByCustomer(this.dataUser.id)
    if (response) {
      this.loading.dismiss();
      this.reservations = response;
    } else {
      this.loading.dismiss();
      this.utils.presentAlert(message.error_load, 'Reserva', '');
    }
  }

}
