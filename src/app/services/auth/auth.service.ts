import {Injectable, EventEmitter} from '@angular/core';
import {RQLogin, RSUser, UserDB} from '../../interfaces/auth';
import {environment} from '../../../environments/environment';
import {HttpClient} from '@angular/common/http';
import {message} from '../../../environments/labels-es';
import {UtilsService} from '../utils/utils.service';
import {HeaderService} from '../header.service';
import {NavController} from '@ionic/angular';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  token: string = null;
  user: RSUser = {};
  dataUser = new EventEmitter<UserDB>();

  constructor(private  http: HttpClient,
              private utils: UtilsService,
              private headers: HeaderService,
              private navController: NavController) {
  }

  login(data: RQLogin) {
    return new Promise(resolve => {
      this.http.post<UserDB>(`${environment.url}/api/auth/login`, data).subscribe(async response => {
          if (response.user) {
            localStorage.setItem('type_document', response.user.type_document);
            localStorage.setItem('identification', response.user.identification);
            await this.saveToken(response.access_token);
            resolve(true);
          } else {
            this.token = null;
            resolve(false);
          }
        }, (() => {
          this.token = null;
          this.utils.presentAlert(message.error_access, 'Ingreso', '');
          resolve(false);
        })
      );
    });
  }

  logout() {
    this.user = {};
    this.token = null;
    localStorage.removeItem('token');
    localStorage.removeItem('type_document');
    localStorage.removeItem('identification');
    this.navController.navigateBack('/login', {animated: true});
  }

  async saveToken(token: string) {
    this.token = token;
    await localStorage.setItem('token', token);
  }

  async loadToken() {
    this.token = await localStorage.getItem('token') || null;
    return this.token;
  }

  async verifyToken(): Promise<boolean> {
    await this.loadToken();
    return new Promise<boolean>(resolve => {
      if (!this.token) {
        this.navController.navigateRoot('/login', {animated: true});
        resolve(false);
      }
      const filter = {
        type_document: localStorage.getItem('type_document'),
        identification: localStorage.getItem('identification')
      };
      this.headers.post(localStorage.getItem('token'), `${environment.url}/api/customer/find`, {...filter}).subscribe(response => {
        if (response.status) {
          this.user = response;
          this.dataUser.emit(response);
          resolve(true);
        } else {
          this.navController.navigateRoot('/login', {animated: true});
          resolve(false);
        }
      }, () => {
        this.navController.navigateRoot('/login', {animated: true});
        resolve(false);
      });
    });

  }
}
