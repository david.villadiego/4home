import {Injectable} from '@angular/core';
import {AlertController, LoadingController, ToastController} from '@ionic/angular';

@Injectable({
  providedIn: 'root'
})
export class UtilsService {

  constructor(private loadingController: LoadingController,
              private alertController: AlertController,
              public toastController: ToastController) {
  }

  async presentLoading(message) {
    const loading = await this.loadingController.create({
      message,
      translucent: true,
    });
    await loading.present();
    return loading;
  }

  async presentToast(message: string, header: string, color: string, duration?: number) {
    const toast = await this.toastController.create({
      header,
      color,
      message,
      position: 'top',
      duration: duration || 2000
    });
    toast.present();
  }

  async presentAlert(message: string, header: string, subHeader: string) {
    const alert = await this.alertController.create({
      cssClass: 'my-custom-class',
      header,
      subHeader,
      message,
      buttons: ['Aceptar']
    });

    await alert.present();
  }

  async presentAlertConfirm(callback, header, message) {
    const alert = await this.alertController.create({
      cssClass: 'my-custom-class',
      header,
      message,
      buttons: [
        {
          text: 'Cancelar',
          role: 'cancel',
          cssClass: 'secondary',
          handler: (blah) => {
            console.log('Confirm Cancel: blah');
          }
        }, {
          text: 'Eliminar',
          handler: () => {
            callback();
          }
        }
      ]
    });

    await alert.present();
  }


  getFormatMonth(month) {

    switch (month) {
      case 0:
        month = '01';
        break;
      case 1:
        month = '02';
        break;
      case 2:
        month = '03';
        break;
      case 3:
        month = '04';
        break;
      case 4:
        month = '05';
        break;
      case 5:
        month = '06';
        break;
      case 6:
        month = '07';
        break;
      case 7:
        month = '08';
        break;
      case 8:
        month = '09';
        break;
      case 9:
        month = '10';
        break;
      case 10:
        month = '11';
        break;
      case 11:
        month = '12';
        break;
    }

    return month;
  }

  getFormatDay(day) {
    let days = day.toString();
    if (days.length === 1) {
      days = '0' + days;
    }
    return days;
  }

  getFormatDate() {
    const date = new Date();
    const day = date.getDate();
    const month = date.getMonth();
    const year = date.getFullYear();
    const dateFormat = `${year}-${this.getFormatMonth(month)}-${this.getFormatDay(day)}`;

    return dateFormat;
  }

  formatDateCompare(date) {
    if (date) {
      const formatDate = new Date(date)
      const result = {
        year: formatDate.getFullYear(),
        month: this.getFormatMonth(formatDate.getMonth()),
        day: this.getFormatDay(formatDate.getDate())
      }

      return result;

    }
  }
}
