import {Injectable} from '@angular/core';
import {HeaderService} from '../header.service';
import {environment} from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ServicetypeService {

  url: string;

  constructor(private headers: HeaderService) {
    this.url = environment.url;
  }

  get() {
    return this.headers.get(localStorage.getItem('token'), `${this.url}/api/servicetype`);
  }

}
