import {Injectable, EventEmitter} from '@angular/core';
import {HeaderService} from '../header.service';
import {environment} from '../../../environments/environment';
import {RQBooking, RSBooking} from '../../interfaces/booking';

@Injectable({
  providedIn: 'root'
})
export class ReservationService {

  url: string;
  newReservation = new EventEmitter<boolean>();

  constructor(private headers: HeaderService) {
    this.url = environment.url;
  }


  getByCustomer(idCustomer: any) {
    return new Promise<any>(async resolve => {
      this.headers.get(localStorage.getItem('token'), `${this.url}/api/reserve/filter/customer/${idCustomer}`).subscribe(response => {
        resolve(response);
      }, erro => resolve(erro));
    });
  }

  getByReference(reference: any) {
    return this.headers.get(sessionStorage.getItem('token'), `${this.url}/api/reserve/filter/reference/${reference}`);
  }

  getByStatus(status: number) {
    return this.headers.get(localStorage.getItem('token'), `${this.url}/api/reserve/filter/status/${status}`);
  }

  post(reserve: RQBooking) {
    return this.headers.post(localStorage.getItem('token'), `${this.url}/api/reserve`, {...reserve});
  }

  put(reserve: RQBooking, id: number) {
    return this.headers.put(localStorage.getItem('token'), `${this.url}/api/reserve/${id}`, {...reserve});
  }

  delete( id: number) {
    return this.headers.delete(localStorage.getItem('token'), `${ this.url}/api/reserve/${id}`);
  }
}
