import {Injectable} from '@angular/core';
import {environment} from 'src/environments/environment';
import {HeaderService} from '../header.service';

@Injectable({
  providedIn: 'root'
})
export class WorkingdayService {

  url: string;

  constructor(private headers: HeaderService) {
    this.url = environment.url;
  }

  findByServiceType(id: string) {
    return this.headers.get(localStorage.getItem('token'), `${this.url}/api/workingday/filter/servicetype/${id}`);
  }
}
