import { Injectable } from '@angular/core';
import {HeaderService} from '../header.service';
import {environment} from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ServiceService {

  url: string;

  constructor(private headers: HeaderService) {
    this.url = environment.url;
  }

  findByTypeAndWorking( type: string, workingDay: string) {
    return this.headers.get(localStorage.getItem('token'), `${ this.url}/api/service/filter/type/${type}/workingday/${workingDay}`);
  }
}
