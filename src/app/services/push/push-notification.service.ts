import {EventEmitter, Injectable} from '@angular/core';
import {OneSignal, OSNotificationPayload, OSNotification} from '@ionic-native/onesignal/ngx';
import {NavController} from '@ionic/angular';

@Injectable({
  providedIn: 'root'
})
export class PushNotificationService {


  mensajes: OSNotificationPayload[] = [];
  userId: string;
  pushListener = new EventEmitter<OSNotificationPayload>();

  constructor(
    private oneSignal: OneSignal,
    private navController: NavController) {
  }

  configuracionIncial() {
    this.oneSignal.startInit('d81df992-fde3-4719-ae90-4213771fdd89', '924862268894');

    this.oneSignal.inFocusDisplaying(this.oneSignal.OSInFocusDisplayOption.Notification);

    this.oneSignal.handleNotificationReceived().subscribe((noti) => {
      // do something when notification is received
      console.log('notificacion recibida', noti);
    });

    this.oneSignal.handleNotificationOpened().subscribe(async (noti) => {
      // do something when a notification is opened
      console.log('notificacion abierta', noti);
      /*setTimeout(() => {
        console.log(noti.notification.payload.additionalData.url);
        this.navController.navigateRoot(noti.notification.payload.additionalData.url, {animated: true});
      }, 1000);*/
    });

    // Obtener el userId del suscriptor
    this.oneSignal.getIds().then(info => {
      this.userId = info.userId;
      console.log(info);
      localStorage.setItem('pushId', this.userId);
    });

    this.oneSignal.endInit();

  }
}
