import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import {HeaderService} from '../header.service';

@Injectable({
  providedIn: 'root'
})
export class SignupService {

  url: string;

  constructor(private headers: HeaderService) {
    this.url = environment.url;
  }

  customerType() {
    return this.headers.get(localStorage.getItem('token'), `${ this.url}/api/singup/customertype`);
  }

  validateIdentification(value: string) {
    return this.headers.get(localStorage.getItem('token'), `${ this.url}/api/singup/customer/filter/identification/${value}`);
  }

  validateEmail(value: string) {
    return this.headers.post(localStorage.getItem('token'), `${ this.url}/api/singup/user/filter/email`, {email: value});
  }

  post( user: any ) {
    return this.headers.post(localStorage.getItem('token'), `${ this.url}/api/singup`, { ...user });
  }

  resetPassword( user: any ) {
    return this.headers.patch(localStorage.getItem('token'), `${ this.url}/api/singup/resetpassword`, { ...user });
  }

  getWithoutAuthentication() {
    return this.headers.get(localStorage.getItem('token'), `${ this.url}/api/singup/documenttype`);
  }
}
