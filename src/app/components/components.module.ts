import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {HeaderComponent} from './header/header.component';
import {BookingsListComponent} from './bookings-list/bookings-list.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {IonicModule} from '@ionic/angular';
import {RouterModule} from '@angular/router';
import {BookingItemComponent} from './booking-item/booking-item.component';
import {NewBookingComponent} from './new-booking/new-booking.component';
import {PipesModule} from '../pipes/pipes.module';
import {PaymentsComponent} from './payments/payments.component';
import {WorkerComponent} from './worker/worker.component';


@NgModule({
  entryComponents: [HeaderComponent, BookingsListComponent, NewBookingComponent, PaymentsComponent, WorkerComponent],
  declarations: [
    HeaderComponent, BookingsListComponent, BookingItemComponent, NewBookingComponent, PaymentsComponent, WorkerComponent
  ],
  exports: [
    HeaderComponent, BookingsListComponent, PaymentsComponent
  ],
  imports: [
    CommonModule,
    IonicModule,
    RouterModule,
    FormsModule,
    ReactiveFormsModule,
    PipesModule
  ]
})
export class ComponentsModule {
}
