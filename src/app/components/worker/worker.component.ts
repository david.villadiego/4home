import {Component, Input, OnInit} from '@angular/core';
import {Professional} from '../../interfaces/booking';

@Component({
  selector: 'app-worker',
  templateUrl: './worker.component.html',
  styleUrls: ['./worker.component.scss'],
})
export class WorkerComponent implements OnInit {
  @Input() professional: Professional;
  year:any
  constructor() { }

  ngOnInit() {

    this.year = new Date(this.professional.admission_date)
    console.log(this.year.getFullYear())
  }

}
