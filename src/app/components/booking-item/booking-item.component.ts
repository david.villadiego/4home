import {Component, Input, OnInit} from '@angular/core';
import {NewBookingComponent} from '../new-booking/new-booking.component';
import {ModalController, PopoverController} from '@ionic/angular';
import {Professional, RSBooking} from '../../interfaces/booking';
import {PaymentsComponent} from '../payments/payments.component';
import {WorkerComponent} from '../worker/worker.component';
import {ReservationService} from '../../services/bookings/reservation.service';
import {UtilsService} from '../../services/utils/utils.service';
import {message} from '../../../environments/labels-es';

@Component({
  selector: 'app-booking-item',
  templateUrl: './booking-item.component.html',
  styleUrls: ['./booking-item.component.scss'],
})
export class BookingItemComponent implements OnInit {
  @Input() booking: RSBooking;
  @Input() flagView: boolean;

  constructor(public modalController: ModalController,
              public popoverController: PopoverController,
              private reservationService: ReservationService,
              private utils: UtilsService) {

  }

  ngOnInit() {
  }

  async editBooking(booking: RSBooking) {
    const modal = await this.modalController.create({
      component: NewBookingComponent,
      presentingElement: await this.modalController.getTop(),
      cssClass: '',
      animated: true,
      showBackdrop: false,
      componentProps: {
        editBooking: booking,
        flagView: this.flagView
      }
    });

    await modal.present();
    modal.onDidDismiss().then((data: any) => {
      this.reservationService.newReservation.emit(data.data);
    });
  }

  async payBooking(booking: RSBooking) {
    const modal = await this.modalController.create({
      component: PaymentsComponent,
      presentingElement: await this.modalController.getTop(),
      animated: true,
      showBackdrop: false,
      componentProps: {
        payment: booking
      }
    });

    modal.present();
  }

  deleteBooking(booking: RSBooking) {

    this.utils.presentAlertConfirm(() => {
      this.reservationService.delete(booking.id).subscribe(data => {

        this.utils.presentAlert( message.success_delete,'Reserva', 'Proceso Exitoso!',);
        this.reservationService.newReservation.emit(true);
      }, (err: any) => {
        this.utils.presentAlert('Error', err.error.message, '');
      });
    }, '¿Esta seguro?', message.comfirm_delete)

  }

  async viewProfessional(professional: Professional) {
    const popover = await this.popoverController.create({
      component: WorkerComponent,
      cssClass: 'popover-view',
      componentProps: {
        professional
      },
      translucent: true,
      animated: true,
    });
    await popover.present();

    const {role} = await popover.onDidDismiss();
    console.log('onDidDismiss resolved with role', role);
  }
}
