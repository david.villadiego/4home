import {Component, Input, OnInit, Output, EventEmitter} from '@angular/core';
import {NewBookingComponent} from '../new-booking/new-booking.component';
import {ModalController} from '@ionic/angular';
import {RSBooking} from '../../interfaces/booking';
import {ReservationService} from '../../services/bookings/reservation.service';

@Component({
  selector: 'app-bookings-list',
  templateUrl: './bookings-list.component.html',
  styleUrls: ['./bookings-list.component.scss'],
})
export class BookingsListComponent implements OnInit {
  @Input() bookingsList: RSBooking[] = [];
  @Input() flagView: boolean;
  @Output() state = new EventEmitter<any>();
  reservation: any[] = [];
  type: string

  constructor(public modalController: ModalController,
              private reservationService: ReservationService) {

  }

  ngOnInit() {
    this.reservation = [
      {label: 'Pre-Agendamientos', value: 1},
      {label: 'Agendamientos sin pago', value: 2},
      {label: 'Pago vencido', value: 3},
      {label: 'Reprogramación', value: 9},
    ]
  }

  async newBooking() {
    const modal = await this.modalController.create({
      component: NewBookingComponent,
      presentingElement: await this.modalController.getTop(),
      animated: true,
      showBackdrop: false,
      componentProps: {
        register: true
      }
    });

    await modal.present();
    modal.onDidDismiss().then((data:any) => {
      this.reservationService.newReservation.emit(data.data);
    });
  }

  changeState() {
    this.state.emit(this.type);
  }
}
