import {Component, Input, OnInit} from '@angular/core';
import {AbstractControl, FormArray, FormBuilder, FormControl, FormGroup, ValidatorFn, Validators} from '@angular/forms';
import {UtilsService} from '../../services/utils/utils.service';
import {message} from '../../../environments/labels-es';
import {WorkingdayService} from '../../services/bookings/workingday.service';
import {ServicetypeService} from '../../services/bookings/servicetype.service';
import {ServiceService} from '../../services/bookings/service.service';
import {RQBooking, RSBooking} from '../../interfaces/booking';
import {ReservationService} from '../../services/bookings/reservation.service';
import {AuthService} from '../../services/auth/auth.service';
import {RSUser} from '../../interfaces/auth';
import {ModalController, NavController} from '@ionic/angular';

@Component({
  selector: 'app-new-booking',
  templateUrl: './new-booking.component.html',
  styleUrls: ['./new-booking.component.scss'],
})
export class NewBookingComponent implements OnInit {

  @Input() editBooking: RSBooking;
  @Input() flagView: boolean;
  loading: any;
  form: FormGroup;
  serviceTypes: Array<any> = new Array();
  workingDays: Array<any> = [];
  services: Array<any> = new Array();
  listServices: Array<any> = new Array();
  addresses: Array<any> = new Array();
  types: Array<any> = [
    {
      value: '1',
      label: 'Esporádico'
    },
    {
      value: '2',
      label: 'Mensualidad'
    }
  ];
  service = null;
  quantity = null;
  price = null;
  daysEdit: Array<any> = [];
  reserve: RQBooking = {};
  days: Array<any> = ['Lunes', 'Martes', 'Miercoles', 'Jueves', 'Viernes', 'Sabado', 'Domingo'];
  dataUser: RSUser = {};
  id: any;
  btnSubmit: string;
  title: string;

  constructor(private formBuilder: FormBuilder,
              public utils: UtilsService,
              private workingdayService: WorkingdayService,
              private servicetypeService: ServicetypeService,
              private serviceService: ServiceService,
              private reservationService: ReservationService,
              private authService: AuthService,
              private navController: NavController,
              private modalControler: ModalController) {
    this.loadForm();
  }

  get daysArray() {
    return this.form.get('days') as FormArray;
  }

  async ngOnInit() {
    this.loading = await this.utils.presentLoading('Cargando');
    if (this.editBooking) {
      this.btnSubmit = 'Editar';
      this.title = 'Editar Reserva';
    } else {
      this.btnSubmit = 'Guardar';
      this.title = 'Crear Reserva';
    }
    this.dataUser = this.authService.user;
    this.getServiceTypes();
    if (this.editBooking) {
      setTimeout(() => {
        this.loadReservation();
      }, 1000);
    }
  }

  regresar() {
    this.modalControler.dismiss(false);
  }

  loadForm() {
    this.form = new FormGroup({
      service_type: new FormControl(null, [Validators.required]),
      working_day: new FormControl(null, [Validators.required]),
      type: new FormControl(null, [Validators.required]),
      service: new FormControl(null, [Validators.required]),
      customer_address: new FormControl(null, [Validators.required]),
      user: new FormControl(null),
      days: this.formBuilder.array([], {validators: this.minDaySelected})
    });

    this.form.get('service_type').valueChanges.subscribe(resp => {
      this.workingDays = new Array();
      this.services = new Array();
      this.form.controls.working_day.reset();
      this.form.controls.type.reset();
      this.form.controls.service.reset();
      this.service = null;
      this.resetDays();
      this.getWorkingDays(resp);
    });

    this.form.get('working_day').valueChanges.subscribe(async resp => {
      this.services = new Array();
      this.form.controls.type.reset();
      this.form.controls.service.reset();
      this.service = null;
      await this.resetDays();
    });

    this.form.get('type').valueChanges.subscribe(async resp => {
      this.services = new Array();
      this.form.controls.service.reset();
      this.service = null;
      await this.resetDays();
    });

    this.form.get('service').valueChanges.subscribe(resp => {
      this.service = this.listServices.find(service => service.id === resp);
      if (this.reserve) {
        this.daysEdit = new Array();
      }
      if (this.form.controls.type.value === '1') {
        this.loadDates();
      }
      if (this.form.controls.type.value === '2') {
        this.loadDays();
      }
    });
  }

  async loadReservation() {
    console.log(this.editBooking)
    this.form.controls.service_type.setValue(this.editBooking.service.working_day.service_type);
    const res = await new Promise<boolean>(resolve => {
      setTimeout(() => {
        this.form.controls.working_day.setValue(this.editBooking.service.working_day.id);
        this.form.controls.type.setValue(this.editBooking.type);
        this.getServices();
        resolve(true)
      }, 1000)
    })
    if (res) {

      setTimeout(() => {
        this.form.controls.service.setValue(this.editBooking.service.id);
        this.form.controls.customer_address.setValue(this.editBooking.customer_address.id);
        this.service = this.listServices.find(service => service.id === this.editBooking.service.id);
        this.daysEdit = this.editBooking.reserve_day;
        if (this.form.controls.type.value === '1') {
          this.loadDates();
        }
        if (this.form.controls.type.value === '2') {
          this.loadDays();
        }

      }, 1000)
    }
  }

  changeTypeState() {
    this.getServices();
  }

  minDaySelected: ValidatorFn = (form: FormArray) => {
    const days = form.controls.map(control => control.value.type === 2);
    const selected = form.controls.map(control => control.value.selected).reduce((prev, next) => next ? prev + next : prev, 0);
    return days.length === 7 ? selected !== this.quantity ? {invalidQuantity: true} : null : null;
  }

  resetDays() {
    this.quantity = null;
    this.price = null;
    this.daysEdit = new Array();
    while (this.daysArray.length !== 0) {
      this.daysArray.removeAt(0)
    }
  }

  async getWorkingDays(serviceType: string) {
    if (serviceType) {
      this.workingdayService.findByServiceType(serviceType).subscribe(resp => {
        resp.data.filter((workingDay: any) => workingDay.status === 1).map((workingDay: any) => {
          this.workingDays.push({value: workingDay.id, label: workingDay.name});
          this.workingDays = this.workingDays.slice();
        });
      }, error => {
        this.loading.dismiss();
        this.utils.presentAlert(message.error_load, 'Reserva', '');
      });
    }
  }

  async getServiceTypes() {
    this.dataUser.customer_address.map((address: any) => {
      this.addresses.push({value: address.id, label: address.address});
      this.addresses = this.addresses.slice();
    });
    this.servicetypeService.get().subscribe((resp: any) => {
      if (resp.data.length > 0) {
        resp.data.filter((serviceType: any) => serviceType.status === 1).map((serviceType: any) => {
          this.serviceTypes.push({value: serviceType.id, label: serviceType.name});
          this.serviceTypes = this.serviceTypes.slice();
        });
        this.loading.dismiss();
      } else {
        this.loading.dismiss();
        this.utils.presentAlert(message.error_load, 'Reserva', '');
      }
    }, error => {
      this.loading.dismiss();
      this.utils.presentAlert(message.error_load, 'Reserva', '');
    });
  }

  async getServices() {
    const type = this.form.controls.type.value;
    const workingDay = this.form.controls.working_day.value;
    if (type && workingDay) {
      this.serviceService.findByTypeAndWorking(type, workingDay).subscribe(resp => {
        this.listServices = resp.data;
        resp.data.filter((service: any) => service.status === 1).map((service: any) => {
          this.services.push({value: service.id, label: service.name});
          this.services = this.services.slice();
          setTimeout(() => {
            this.loading.dismiss();
          }, 1000)
        });
      }, error => {
        this.loading.dismiss();
        this.utils.presentAlert(message.error_load, 'Reserva', '');
      });
    }
  }

  loadDates() {
    if (this.service) {
      while (this.daysArray.length !== 0) {
        this.daysArray.removeAt(0)
      }
      this.quantity = this.service.quantity;
      this.price = this.quantity * this.service.price;
      if (this.daysEdit.length > 0) {
        this.daysEdit.map(date => {
          const reserveDate = date.date.split('-');
          this.daysArray.push(
            this.formBuilder.group({
              index: new FormControl(this.daysArray.controls.length),
              date: new FormControl(date.date, [Validators.required, this.validateDate.bind(this)]),
              type: 1,
              datetime: date.date
            })
          );
          console.log(this.daysArray.controls[0])
        });
      } else {
        for (let i = 0; i < this.quantity; i++) {
          this.daysArray.push(
            this.formBuilder.group({
              index: new FormControl(this.daysArray.controls.length),
              date: new FormControl(null, [Validators.required, this.validateDate.bind(this)]),
              type: 1
            })
          )
        }
      }
    }
  }

  loadDays() {
    if (this.service) {
      while (this.daysArray.length !== 0) {
        this.daysArray.removeAt(0)
      }
      this.quantity = this.service.quantity;
      if (this.service.type === 1) {
        this.price = this.quantity * this.service.price;
      } else {
        if (this.service.type === 2) {
          this.price = (this.quantity * this.service.price) * 4;
        }
      }
      this.days.map(day => {
        const editDay = this.daysEdit.find(days => days.day === this.daysArray.controls.length);
        this.daysArray.push(
          this.formBuilder.group({
            index: new FormControl(this.daysArray.controls.length),
            day: new FormControl(day),
            selected: new FormControl(editDay === undefined ? false : true),
            type: 2
          })
        )
      });
    }
  }

  isWeekend(date) {
    if (date) {
      const d = new Date(date.year, date.month - 1, date.day);
      return d.getDay() === 0 || d.getDay() === 6;
    }
  }

  validateDate(control: AbstractControl) {
    const date = this.utils.formatDateCompare(control.value);
    let exist = false;
    if (this.daysArray.length > 0 && !control.pristine) {
      this.daysArray.controls.map(o => {
        const dateArray = this.utils.formatDateCompare(o.value.date);
        if (dateArray != null) {
          if (dateArray.year === date.year &&
            dateArray.month === date.month &&
            dateArray.day === date.day
          ) {
            exist = true;
          }
        }
      });
    }
    const repeatedDate = exist ? {'repeatedDate': true} : null;
    if (repeatedDate) {
      this.utils.presentToast(message.error_equals_date, 'Reserva', 'warning', 10000);
    }
    return repeatedDate
  }

  cancel() {
    this.daysEdit = new Array();
    this.id = null;
    this.reserve = {};
    this.form.reset();
    while (this.daysArray.length !== 0) {
      this.daysArray.removeAt(0);
    }
  }

  async onSubmit() {
    this.loading = await this.utils.presentLoading('Cargando');
    this.reserve = this.form.value;
    this.reserve.user = this.dataUser.id;

    const listDays = new Array();

    this.daysArray.value.map((day: any) => {

      if (day.type === 1) {
        const date = new Date(day.date);
        listDays.push({
          date: `${date.getFullYear()}-${date.getMonth() + 1}-${date.getDate()}`
        });
      }

      if (day.type === 2 && day.selected) {
        listDays.push({
          day: day.index++
        });
      }

    });
    this.reserve.days = listDays;
    console.log(this.reserve)
    if (this.editBooking) {
      this.reservationService.put(this.reserve, this.editBooking.id).subscribe((data: any) => {
        console.log(data)
        this.loading.dismiss();
        this.utils.presentAlert(message.success_update_booking, 'Reserva', '¡Proceso Exitoso!');
        this.cancel();
        this.modalControler.dismiss(true);
      }, (err) => {
        if (err.error.errors) {
          this.loading.dismiss();
          let mensage = '';
          Object.keys(err.error.errors).forEach((data, index) => {
            mensage += err.error.errors[data][0] + '<br>';
          });
          this.utils.presentAlert(mensage, 'Reserva', 'Error');
        } else {
          this.loading.dismiss();
          if (err.status === 401) {
            this.navController.navigateRoot('/login', {animated: true});
          }
        }
      });

    } else {
      this.reservationService.post(this.reserve).subscribe((data: any) => {
        this.loading.dismiss();
        this.utils.presentAlert(message.success_booking, 'Reserva', '¡Proceso Exitoso!');
        this.cancel();
        this.modalControler.dismiss(true);
      }, (err) => {
        this.loading.dismiss();
        if (err.error.errors) {
          let mensage = '';

          Object.keys(err.error.errors).forEach((data, index) => {
            mensage += err.error.errors[data][0] + '<br>';
          });
          this.utils.presentAlert(mensage, 'Reserva', 'Error');
        } else {
          this.loading.dismiss();
          if (err.status === 401) {
            this.navController.navigateRoot('/login', {animated: true});
          }
        }
      });
    }
  }


}
