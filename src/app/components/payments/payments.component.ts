import {Component, Input, OnInit} from '@angular/core';
import {ModalController} from '@ionic/angular';
import {FormGroup} from '@angular/forms';
import {AuthService} from '../../services/auth/auth.service';
import {RSUser} from '../../interfaces/auth';
import {PaymentModel} from '../../interfaces/payment';
import {RSBooking} from '../../interfaces/booking';
import {environment} from '../../../environments/environment';
import {Md5} from 'md5-typescript';

@Component({
  selector: 'app-payments',
  templateUrl: './payments.component.html',
  styleUrls: ['./payments.component.scss'],
})
export class PaymentsComponent implements OnInit {

  urlPayu = environment.payu;
  responseUrl = environment.responseUrl;
  confirmationUrl = environment.confirmationUrl;
  dataUser: RSUser = {};
  @Input() payment: RSBooking;
  days: Array<any> = ['Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado', 'Domingo'];
  daySelect = [];
  paymentPayu: PaymentModel = new PaymentModel();
  promocode = null;
  subtotal = 0;
  total = 0;
  canPay = false;

  constructor(private modalControler: ModalController, private authService: AuthService,) {
  }

  ngOnInit() {
    this.dataUser = this.authService.user;
    this.loadReserve();
  }

  regresar() {
    this.modalControler.dismiss();
  }

  onSubmit() {
    console.log(this.paymentPayu)
  }

  loadReserve() {
    if (this.payment.reference) {
      const comparationDate = new Date(this.payment.scheduling_date);
      comparationDate.setDate(comparationDate.getDate() + 1);
      const now = new Date();
      const diffInMs = Date.parse(comparationDate.toString()) - Date.parse(now.toString());
      const diffHrs = Math.floor((diffInMs % 86400000) / 3600000);
      const diffMins = Math.round(((diffInMs % 86400000) % 3600000) / 60000);
      this.canPay = diffHrs > 0 ? true : false;
      this.payment.limit = diffHrs + ' horas ' + diffMins + ' minutos ';
      if (this.payment.type === 2) {
        this.payment.reserve_day.map((day: any) => this.daySelect.push(this.days[day.day]));
      }
      this.loadInformationPayment();
    }
  }

  loadInformationPayment() {
    this.paymentPayu.payerDocument = this.dataUser.identification;
    this.paymentPayu.buyerFullName = `${this.dataUser.name} ${this.dataUser.lastname}`;
    this.paymentPayu.buyerEmail = this.dataUser.email;
    this.paymentPayu.telephone = this.dataUser.phone;
    let amount = '0';
    if (this.payment.type === 1) {
      amount = String(this.payment.service.price * this.payment.service.quantity);
    } else {
      if (this.payment.type === 2) {
        amount = String((this.payment.service.price * this.payment.service.quantity) * 4);
      }
    }
    const reference = new Date().getTime().toString().slice(-5) + this.payment.reference;
    this.paymentPayu.merchantId = environment.merchantId;
    this.paymentPayu.accountId = environment.accountId;
    this.paymentPayu.description = 'Compra de servicios 4Home S.A.S. Servicio con referencia #' + this.payment.reference;
    this.paymentPayu.referenceCode = reference
    this.paymentPayu.amount = amount;
    this.paymentPayu.tax = '0';
    this.paymentPayu.taxReturnBase = '0';
    this.paymentPayu.currency = 'COP';
    this.paymentPayu.signature = Md5.init(environment.apiKey + '~' + environment.merchantId + '~' + reference + '~' + amount + '~COP');
    this.paymentPayu.test = '1';
    this.paymentPayu.extra1 = this.payment.reference;
    this.paymentPayu.extra2 = this.payment.id;
    this.subtotal = parseInt(amount, 0);
    this.total = parseInt(amount, 0);
  }
}
