import {Component, OnInit} from '@angular/core';
import {AuthService} from './services/auth/auth.service';
import {RSUser} from './interfaces/auth';
import {PushNotificationService} from './services/push/push-notification.service';
import {Platform} from '@ionic/angular';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss'],
})
export class AppComponent implements OnInit {
  public appPages = [
    {title: 'Reservas', url: 'bookings', icon: 'people'},
    {title: 'Historial', url: 'history', icon: 'list'},
  ];

  user: RSUser = {};

  constructor(private authService: AuthService,
              private pushServices: PushNotificationService,
              private platform: Platform) {
    this.initializeApp();
  }

  ngOnInit() {
    this.authService.dataUser.subscribe(data => {
      console.log(data)
      this.user = data
    })
  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.pushServices.configuracionIncial();
    });
  }

  logout() {
    this.authService.logout();
  }
}
