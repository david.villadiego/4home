/*** Listar reserva ***/

export interface User {
  id: number;
  type_document: number;
  identification: string;
  name: string;
  lastname: string;
  email: string;
  age?: any;
  phone: string;
  mobile: string;
  contact_name: string;
  address?: any;
  billing_address: string;
  customer_type: number;
  reset_password: number;
  status: number;
  created_at: Date;
  updated_at: Date;
}

export interface CustomerAddress {
  id: number;
  user: number;
  address: string;
  created_at: Date;
  updated_at: Date;
}

export interface WorkingDay {
  id: number;
  name: string;
  init_hour: string;
  end_hour: string;
  service_type: number;
  status: number;
  created_at: Date;
  updated_at: Date;
}

export interface Service {
  id: number;
  name: string;
  price: number;
  working_day: WorkingDay;
  description: string;
  type: number;
  quantity: number;
  status: number;
  created_at: Date;
  updated_at: Date;
}

export interface Professional {
  id: number;
  type_document: number;
  identification: string;
  name: string;
  lastname: string;
  age: number;
  address: string;
  phone: string;
  phone_contact: string;
  salary: string;
  email: string;
  photo: string;
  admission_date: string;
  retirement_date?: any;
  position: number;
  status: number;
  created_at: Date;
  updated_at: Date;
}

export interface Supervisor {
  id: number;
  type_document: number;
  identification: string;
  name: string;
  lastname: string;
  age: number;
  address: string;
  phone: string;
  phone_contact: string;
  salary: string;
  email: string;
  photo: string;
  admission_date: string;
  retirement_date?: any;
  position: number;
  status: number;
  created_at: Date;
  updated_at: Date;
}

export interface ReserveDay {
  id: number;
  reserve: number;
  day: number;
  date: string;
  created_at: Date;
  updated_at: Date;
}

export interface RSBooking {
  id?: number;
  reference?: string;
  user?: User;
  customer_address?: CustomerAddress;
  service?: Service;
  type?: number;
  status?: number;
  professional?: Professional;
  supervisor?: Supervisor;
  scheduling_date?: string;
  created_at?: Date;
  updated_at?: Date;
  reserve_day?: ReserveDay[];
  limit: string;
}

/*** Crear reserva ***/
export interface RQBooking {
  service_type?: number;
  working_day?: number;
  type?: number;
  service?: number;
  customer_address?: number;
  user?: number;
  days?: Day[];
}

export interface Day {
  date: string;
}
