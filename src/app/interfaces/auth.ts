/*** Request servicio login ***/
export interface RQLogin {
  email?: string;
  password?: string;
}
/*** Respuesta servicio login ***/
export interface User {
  id?: number;
  type_document?: string;
  identification?: string;
  name?: string;
  lastname?: string;
  email?: string;
  age?: any;
  phone?: string;
  mobile?: string;
  contact_name?: string;
  address?: any;
  billing_address?: string;
  customer_type?: number;
  reset_password?: any;
  status?: number;
  created_at?: Date;
  updated_at?: Date;
}

export interface UserDB {
  access_token?: string;
  token_type?: string;
  expires_at?: string;
  user?: User;
}
/*** Request servicio registrarse ***/
export class RQRegister {
  type_document: string;
  identification: string;
  name: string;
  lastname: string;
  email: string;
  password: string;
  age: string;
  address: string;
  phone: string;
  roles: any;
  mobile: string;
  status: boolean;
  contact_name: string;
  billing_address: string;
  customer_type: string;
  addresses: any;
}

/*** Respuesta del servivio obtener usuarios ***/
export interface RSUser {
  id?: number;
  type_document?: number;
  identification?: string;
  name?: string;
  lastname?: string;
  email?: string;
  age?: any;
  phone?: string;
  mobile?: string;
  contact_name?: string;
  address?: any;
  billing_address?: string;
  customer_type?: number;
  reset_password?: number;
  status?: number;
  created_at?: Date;
  updated_at?: Date;
  customer_address?: CustomerAddress[];
}

export interface CustomerAddress {
  id: number;
  user: number;
  address: string;
  created_at: Date;
  updated_at: Date;
}
