export class PaymentModel {
  merchantId: string;
  accountId: string;
  description: string;
  referenceCode: string;
  amount: string;
  tax: string;
  taxReturnBase: string;
  currency: string;
  signature: string;
  test: string;
  buyerEmail: string;
  buyerFullName: string;
  payerDocument: string;
  telephone: string;
  responseUrl: string;
  confirmationUrl: string;
  extra1: string;
  extra2: number;
  extra3: string;
}
